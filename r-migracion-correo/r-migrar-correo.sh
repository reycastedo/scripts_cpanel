#!/bin/bash

################################################################################
#
# Script para crear cuentas de correo de forma automática leyendo desde un fichero en el formato solicitado a los clientes
#
# Uso:
#   ./r-crear-buzones.sh usuario_CPanel fichero_correos [separador]
#
# Dependencias no estándar:
# uapi
# whmapi
#
# Fecha creación: 2018-11-06
#
# Autor: fran.rc@raiolanetworks.es
#
################################################################################


# Escribe la ayuda del programa y lo finaliza
# Recibe: nada
# Devuelve: nada
function ayuda() {
    echo "IMPORTAR"
    echo "Uso: ${0} --import cpuser archivo-cuentas-email [separador]"
    echo
    echo "EXPORTAR"
    echo "Uso: ${0} --export cpuser"
    exit 1
}

# Genera una contraseña aleatoria de longidud "longitud" creada con los caracteres en el array "char"
# Recibe: nada
# Devuelve: contraseña
function clave_aleatoria() {
    local longitud
    local char
    local max
    local i
    local rand
    local password

    longitud=12
    char=(0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ! \# % ^ \&)
    max=${#char[*]}

    for (( i = 1; i <= longitud ; i++ ))
    do
        rand="${RANDOM}%${max}"
        password="${password}${char[$rand]}"
    done

    echo "${password}"
}

# Comprueba si un string pasado como argumento coincide con un usuario CPanel
# Recibe: string
# Devuelve: nada
function existe_usuario() {
    local usuario

    usuario="${1}"

    if ! whmapi1 --output=xml list_users | grep -Foq "<users>${usuario}</users>"
    then
        echo "$( tput setaf 1 )El usuario ${usuario} CPanel no existe$( tput sgr 0 )"
        exit 1
    fi
}

# Verifica si un dominio pertenece un usuario CPanel
# Recibe:
# |- arg1: string, usuario CPanel
# |- arg2: array, dominios
# Devuelve:
# |- si los dominios pertenecen al usuario, nada
# |- en caso contrario, finaliza el scritp y devuelve un string con un mensaje de error y los dominios que no pertenecen al usuario
function dominios_pertenecen_usuario() {
    local usuario
    local dominios_a_buscar
    local main_domain
    local addon_domains
    local alias_domains
    local subdomains
    local dominios_cpanel
    local dominios_no_pertenecen_usuario
    local dominio_cpanel
    local dominio_correo
    local i

    usuario="${1}"
    shift
    dominios_a_buscar=(${@})

    mapfile -t main_domain < <( uapi --user="$usuario" DomainInfo list_domains | grep -F 'main_domain' | sed -n -e 's/.*: //p' )
    mapfile -t addon_domains < <( uapi --user="$usuario" DomainInfo list_domains | sed -n '/addon_domains/,/main_domain/{/addon_domains/b;/main_domain/b;p}' | sed -n -e 's/.*- //p' )
    mapfile -t alias_domains < <( uapi --user="$usuario" DomainInfo list_domains | sed -n '/parked_domains/,/sub_domains/{/parked_domains/b;/sub_domains/b;p}' | sed -n -e 's/.*- //p' )
    mapfile -t subdomains < <( uapi --user="$usuario" DomainInfo list_domains | sed -n '/sub_domains/,/errors/{/sub_domains/b;/errors/b;p}' | sed -n -e 's/.*- //p' )

    dominios_cpanel=("${main_domain[@]}" "${addon_domains[@]}" "${alias_domains[@]}" "${subdomains[@]}")

    declare -a dominios_no_pertenecen_usuario

    for dominio_correo in "${dominios_a_buscar[@]}"
    do
        (
        for dominio_cpanel in "${dominios_cpanel[@]}"
        do
            if [[ "${dominio_cpanel}" == "${dominio_correo}" ]]
            then
                exit 0
            fi
        done
        exit 1
        )

        if [[ "${?}" -ne 0 ]]
        then
            dominios_no_pertenecen_usuario+=("${dominio_correo}")
        fi
    done

    if [[ "${#dominios_no_pertenecen_usuario[@]}" -ne 0 ]]
    then
        echo "$( tput setaf 1 )El / Los siguientes dominios no pertenecen al usuario ${usuario}$( tput sgr 0 )"
        for i in "${dominios_no_pertenecen_usuario[@]}"
        do
            echo "${i}"
        done
        exit 1
    fi
}

# Comprueba si existe un buzón de correo:
# - si existe, pide confirmación al usuario para cambiar la contraseña
# - si no existe, llama a la función de creación
# Recibe:
# |- arg1: usuario CPanel
# |- arg2: buzon de correo
# |- arg3: contraseña del buzón
# |- arg4: dominio al que irá asociado el buzón
# Devuelve: nada
function existe_buzon() {
    local usuario
    local buzon
    local clave
    local dominio
    local buzon_existe
    local input

    usuario="${1}"
    buzon="${2}"
    clave="${3}"
    dominio="${4}"

    echo -n "${buzon}@${dominio}${separador}${clave}${separador}" >> "${fichero_patata}"

    buzon_existe=$( uapi --user="${usuario}" Email list_pops regex="${buzon}@${dominio}" 2>/dev/null )

    if grep -Fq "${buzon}@${dominio}" <<< "${buzon_existe}"
    then
        echo "La cuenta de correo ya existe: ${buzon}@${dominio}"

        while true
        do
            read -p "¿Quieres cambiarle la contraseña?: [s/n] " input < /dev/tty
            case "${input}" in
                "s"|"y"|"S"|"Y"|"si"|"SI"|"Si"|"yes"|"YES"|"Yes" )
                    modificar_clave_buzon "${usuario}" "${buzon}" "${clave}" "${dominio}"
                    return 0
                    ;;
                "n"|"N"|"no"|"NO"|"No" )
                    echo >> "${fichero_patata}"
                    return 1
                    ;;
                * )
                    echo "Responde si o no"
                    ;;
            esac
        done
    else
        crear_buzon "${usuario}" "${buzon}" "${clave}" "${dominio}"
        return "${?}"
    fi
}

# Cambia la contraseña de un buzón de correo, hace backup de la clave anterior
# Recibe:
# |- arg1: usuario CPanel
# |- arg2: buzon de correo
# |- arg3: contraseña del buzón
# |- arg4: dominio al que irá asociado el buzón
# Devuelve:
# |- nada, escribe en el fichero temporal la cuenta de correo y la clave que le ha asignado a dicha cuenta
function modificar_clave_buzon() {
    local usuario
    local buzon
    local clave
    local dominio
    local backup_file
    local cambiar_clave

    usuario="${1}"
    buzon="${2}"
    clave="${3}"
    dominio="${4}"

    backup_file="/home/${usuario}/etc/${dominio}/raiola.shadow.back"

    # Se crea backup de la clave antes de cambiarla
    ! [[ -f "${backup_file}" ]] && touch "${backup_file}"
    ! grep -Fq "$( date "+%F %H:%M" )" "${backup_file}" && ( date "+%F %H:%M" >> "${backup_file}" )
    grep "^${buzon}:.*" "/home/${usuario}/etc/${dominio}/shadow" >> "${backup_file}"

    # Se cambia la contraseña
    cambiar_clave=$( uapi --user="${usuario}" Email passwd_pop email="${buzon}" password="${clave}" domain="${dominio}" 2>/dev/null )

    # De no cumplir los requisitos de seguridad, se genera una aleatoria
    if grep -Fq "Please enter a password with a strength rating of" <<< "${cambiar_clave}"
    then
        clave=$( clave_aleatoria )
        uapi --user="${usuario}" Email passwd_pop email="${buzon}" password="${clave}" domain="${dominio}" 1>/dev/null 2>/dev/null
    fi

    echo "${buzon}@${dominio}${separador}${clave}" >> "${fichero_patata}"

    buzones_clave_modificada+=( "${buzon}@${dominio}" )
}

# Verifica si una cuenta de correo existe y de no ser así la crea
# Recibe:
# |- arg1: usuario CPanel
# |- arg2: buzon de correo
# |- arg3: contraseña del buzón
# |- arg4: dominio al que irá asociado el buzón
# Devuelve:
# |- nada, escribe en el fichero temporal el buzón de correo creado y la clave asignada
# |- en caso de error, string notificando dicho error
function crear_buzon() {
    local usuario
    local buzon
    local clave
    local dominio
    local crear_cuenta
    local buzon_existe

    usuario="${1}"
    buzon="${2}"
    clave="${3}"
    dominio="${4}"

    # Se crea la el buzón de correo
    crear_cuenta=$( uapi --user="${usuario}" Email add_pop email="${buzon}" password="${clave}" quota=0 domain="${dominio}" skip_update_db=1 2>/dev/null )
    if grep -Fq "Please enter a password with a strength rating of" <<< "${crear_cuenta}"
    then
        # En el caso de que la contraseña no sea suficientemente fuerte, se genera una nueva y se crea el buzón
        clave=$( clave_aleatoria )
        uapi --user="${usuario}" Email add_pop email="${buzon}" password="${clave}" quota=0 domain="${dominio}" skip_update_db=1 &>/dev/null
        buzones_clave_modificada+=( "${buzon}@${dominio}" )
    fi

    # Se comprueba que el buzón existe
    buzon_existe=$( uapi --user="${usuario}" Email list_pops regex="${buzon}@${dominio}" 2>/dev/null )
    if grep -Fq "${buzon}@${dominio}" <<< "${buzon_existe}"
    then
        echo "${buzon}@${dominio}${separador}${clave}" >> "${fichero_patata}"
        return 0
    else
        echo >> "${fichero_patata}"
        echo -e "\n$( tput setaf 1 )No se ha podido crear la cuenta ${buzon}@${dominio}$( tput sgr 0 )"
        buzones_erroneos+=( "${buzon}@${dominio}" )
        return 2
    fi
}

# Verifica lista los buzones de correo para un usuario CPanel
# Recibe:
# |- arg1: usuario CPanel
# Devuelve:
# |- listado de buzones del usuario
function listar_cuentas_correo() {
    local usuario

    usuario="${1}"

    uapi --user="${usuario}" Email list_pops 2>/dev/null | grep email | cut -d : -f 2
}


main() {
    # Se comprueba que el script se llama con el número de parámetros correctos
    if [[ "${#}" -lt 2 ]] && [[ "${#}" -gt 4 ]]
    then
        ayuda
    fi

    case "${1}" in

        --export)

            shift

            if [[ "${#}" -ne 1 ]]
            then
                ayuda
            fi

            usuario="${1}"

            existe_usuario "${usuario}"

            listar_cuentas_correo "${usuario}"
            exit 0
        ;;

        --import)

            shift

            if [[ "${#}" -ne 2 ]] && [[ "${#}" -ne 3 ]]
            then
                ayuda
            fi

            usuario="${1}"
            archivo_configuracion="${2}"
            separador="${3:-;}"

            # Se comprueba la existencia del archivo pasado como argumento posicional 2
            if ! [[ -f "${archivo_configuracion}" ]]
            then
                echo "$( tput setaf 1 )El archivo de configuración indicado no existe$( tput sgr 0 )"
                exit 1
            fi

            # Se comprueba que el argumento definido como separador no ha sido pasado más de dos veces por línea
            mal_uso_separador=0
            while read -r line
            do
                num_ocurrencias=$( grep -o "${separador}" <<< "${line}" | wc -l )
                if [[ "${num_ocurrencias}" -ne 1 ]]
                then
                    if [[ "${num_ocurrencias}" -eq 0 ]]
                    then
                        continue
                    else
                        mal_uso_separador=1
                        echo "${line}"
                    fi
                fi
            done < "${archivo_configuracion}"

            if [[ "${mal_uso_separador}" -ne 0 ]]
            then
                echo "$( tput setaf 1 )Estás usando el separador varias veces en la o las líneas anteriores, arréglalo$( tput sgr 0 )"
                exit 1
            fi

            # Se comprueba la existencia del usuario pasado como argumento en posición 1
            existe_usuario "${usuario}"

            # Se obtienen los dominios del fichero pasado como argumento 2 para crear las cuentas de email
            declare -a dominios_correo
            mapfile -t dominios_correo < <( awk -F"${separador}" '{print $1}' < "${archivo_configuracion}" | cut -d @ -f 2 | sort | uniq )

            dominios_pertenecen_usuario "${usuario}" "${dominios_correo[@]}"

            # Se crea un fichero temporal para poder guardar las cuentas de correo creadas con la respectiva contraseña
            fichero_patata=$( mktemp "/tmp/${usuario}.XXXXX" )

            # Array para llevar un control de los buzones a los que se les cambia la clave
            declare -a buzones_clave_modificada

            # Array para llevar control de los buzones que no han podido ser creados
            declare -a buzones_erroneos

            # Se crean las cuentas de email
            while IFS="${separador}" read cuenta_correo clave
            do
                # Se comprueba que el string tiene un formato de cuenta de correo
                # Al haber comprobado antes que los dominios pertenecen al usuario no hace falta más
                if ! [[ "${cuenta_correo}" =~ @ ]] || [[ "${cuenta_correo}" =~ ^# ]]
                then
                    continue
                fi

                buzon=$( echo "${cuenta_correo}" | cut -d @ -f 1 )
                dominio=$( echo "${cuenta_correo}" | cut -d @ -f 2 )

                existe_buzon "${usuario}" "${buzon}" "${clave}" "${dominio}"

                salida="${?}"

                if [[ "${salida}" -eq 1 ]]
                then
                    no_cambiar_clave=1
                elif [[ "${salida}" -eq 2 ]]
                then
                    error_crear_buzon=1
                fi
            done < "${archivo_configuracion}"

            # En el caso de que haya fallado la creación de alguna de las cuentas de email o alguna ya existiese, se finaliza la ejecución
            if [[ "${error_crear_buzon}" -eq 1 ]]
            then
                echo -e "\n$( tput setaf 1 )Problema al crear alguno de los buzones de correo:$( tput sgr 0 )"
                cat "${fichero_patata}"
            elif [[ "${no_cambiar_clave}" -eq 1 ]]
            then
                echo -e "\n$( tput setaf 3 )Has elegido no modificar la clave de un buzón preexistente, fichero de patata incompleto:$( tput sgr 0 )"
                cat "${fichero_patata}"
            else
                # Si todas las cuentas de correo se han creado correctamente se imprime el fichero para poder usar en patata
                echo -e "\n$( tput setaf 2 )Fichero para patata:$( tput sgr 0 )"
                cat "${fichero_patata}"
            fi

            if [[ "${#buzones_clave_modificada[@]}" -ne 0 ]]
            then
                echo -e "\n$( tput setaf 3 )Es necesario informar al cliente que la clave de los siguientes buzones será distinta en destino:$( tput sgr 0 )"
                for cuenta_correo in "${buzones_clave_modificada[@]}"
                do
                    grep "^${cuenta_correo}${separador}" "${fichero_patata}" | awk -F"${separador}" '{print $3 " " $4}'
                done
            fi

            rm "${fichero_patata}"
            rm "${archivo_configuracion}"
            [[ "${error_crear_buzon}" -eq 1 ]] || [[ "${no_cambiar_clave}" -eq 1 ]] && exit 1
            exit 0
        ;;

        *)
            ayuda
        ;;
    esac
}

main "$@"
