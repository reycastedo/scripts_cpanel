#!/bin/bash

# ========================= Configuración ==========================================

dominios="/root/ip_reseller_bash/dominios"                                           # Dominios con IP de correo exclusiva en formato "dominio: ip"
cpanels="/root/ip_reseller_bash/cpanels"                                             # Cuentas CPanel con IP de correo asignada en formato "usuario_cpanel: ip"
resellers="/root/ip_reseller_bash/resellers"                                         # Resellers con dirección IP contratada en formato "reseller: ip"

fecha=$( date +%Y-%m-%d:%H:%M:%S )

exim_backup_config_files="/etc/exim_backup_files"                                    # Directorio en donde se guardarán los ficheros de exim mailhelo y mailips si es necesario rotarlos

servidor_dns="localhost"                                                             # Servidor DNS contra el que se hará la resolución de los registros TXT, por defecto en los NET, localhost

# ========================== Fin Configuración =======================================

# ========================== Funciones auxiliares ====================================

# Escribe el par dominio/IP dominio/mailhelo en el fichero de configuración correspondiente
# Recibe:
# |- arg1 = dominio
# |- arg2 = IP
# Devuelve: nada
function escribe_archivo_configuracion() {
    local dominio="${1}"
    local ip="${2}"

    echo "${dominio}: ${ip}" >> "${tmp_mailips}"
    echo "${dominio}: mail.${dominio}" >> "${tmp_mailhelo}"
}

# Comprueba si el dominio dado tiene un registro SPF válido en el propio servidor o es necesario modificarlo. En el último caso, hace la gestión.
# Recibe:
# |- arg1 = dominio
# |- arg2 = IP
# |- arg3 = usuario cpanel
# Devuelve: nada
function regenerar_spf() {
    local dominio="${1}"
    local ip="${2}"
    local usuario_cpanel="${3}"
    local prefijo="${4}"  # Necesario en caso de que un subdominio tenga un registro SPF

    local txt_incorrecto
    local entradas_txt
    local valor_actual_spf
    local nuevo_valor_spf
    local sufijo
    local valor_spf_codificado

    local entrada
    local i

    txt_incorrecto=$( dig +short "${prefijo}${dominio}" TXT @"${servidor_dns}" | grep -F "v=spf1" | grep -Fv "${ip}" ) # En caso de guardar contenido, el registro TXT "v=spf1..." no contendría la IP de envío definida

    if [[ -n "${txt_incorrecto}" ]];
    then
        entradas_txt=$( cpapi2 --user="${usuario_cpanel}" ZoneEdit fetchzone_records domain="${dominio}" customonly=0 name="${prefijo}${dominio}." class=IN type=TXT | grep -F "Line" | awk '{print $2}' )
        for entrada in ${entradas_txt}; # Previene errores en caso de varios registros spf en un dominio
        do
            valor_actual_spf=$( cpapi2 --user="${usuario_cpanel}" ZoneEdit fetchzone_records domain="${dominio}" customonly=0 line="${entrada}" name="${prefijo}${dominio}." class=IN type=TXT | grep -F "record: v=spf1" | grep -o "v=spf1 .*" )
            if [[ -n "${valor_actual_spf}" ]];
            then
                for i in ${valor_actual_spf};
                do
                    if ! [[ "${i}" =~ (\-|\?|\~)all$ ]];
                    then
                        nuevo_valor_spf+="${i} "
                    else
                        sufijo="${i}"
                    fi
                done

                nuevo_valor_spf+="+ip4=${ip} "
                nuevo_valor_spf+="${sufijo}"
            fi
        done

        valor_spf_codificado=$( echo -n "${nuevo_valor_spf}" | perl -MURI::Escape -ne 'print uri_escape($_)' )
        cpapi2 --user="${usuario_cpanel}" ZoneEdit edit_zone_record Line="${entrada}" domain="${dominio}" name="${prefijo}${dominio}." type=TXT txtdata="${valor_spf_codificado}" class=IN &>/dev/null
    fi
}

# Configura dos variables globales: 
# 1. un array con el dominio princial, dominios adicionales y alias del usuario CPanel dado
# 2. un segundo array con los subdominios del usuario CPanel pasado como argumento
# Recibe: usuario CPanel
# Devuelve: las dos variables globales descritas anteriormente
function obtener_dominios_cpanel() {
    local user="$1"
    local main_domain
    local addon_domains
    local alias_domains
    local subdomains

    mapfile -t main_domain < <( uapi --user="$user" DomainInfo list_domains | grep -F 'main_domain' | sed -n -e 's/.*: //p' )
    mapfile -t addon_domains < <( uapi --user="$user" DomainInfo list_domains | sed -n '/addon_domains/,/main_domain/{/addon_domains/b;/main_domain/b;p}' | sed -n -e 's/.*- //p' )
    mapfile -t alias_domains < <( uapi --user="$user" DomainInfo list_domains | sed -n '/parked_domains/,/sub_domains/{/parked_domains/b;/sub_domains/b;p}' | sed -n -e 's/.*- //p' )
    mapfile -t subdomains < <( uapi --user="$user" DomainInfo list_domains | sed -n '/sub_domains/,/errors/{/sub_domains/b;/errors/b;p}' | sed -n -e 's/.*- //p' )

    dominios_cpanel=("${main_domain[@]}" "${addon_domains[@]}" "${alias_domains[@]}")
    subdominios=("${subdomains[@]}")
}

# Busca a qué dominio pertenece un subdominio y devuelve como variables globales el prefijo que conforma el subdominio y el dominio
# Recibe:
# |- arg1 = array con dominios
# |- arg2 = subdominio para el que buscamos la coincidencia
# Devuelve:
# |- variable global dominio
# |- variable global prefijo, correspondiente al prefijo que completa el dominio para crear el subdominio completo
function match() {
    local dominios=( "${@}" )
    local subdominio=${dominios[$(( ${#array[@]} - 1))]}
    unset dominios[$(( ${#dominios[@]} - 1))]

    for dominio in ${dominios[@]}; do
        if [[ ${subdominio} =~ .*.${dominio}$ ]]; then
            dominio="$dominio"
            prefijo=${subdominio%${dominio}}
            return
        fi
     done
}

# Para cada uno de los dominios / subdominios de una cuenta CPanel dada, decide si es necesario escribirlos al fichero de configuración de exim y regenerar su spf
# Recibe:
# |- arg1 = usuario_cpanel
# |- arg2 = ip
# Devuelve: nada
function bucle_principal {
    local usuario_cpanel="$1"
    local ip="$2"
    local dominio
    local subdominio

    obtener_dominios_cpanel "${usuario_cpanel}"

    # Se envían los dominios a la función de creación de ficheros de configuración, si no han sido escritos previamente, los define
    for dominio in "${dominios_cpanel[@]}";
    do
        if ! grep -Fq "${dominio}" "${tmp_mailips}";
        then
            escribe_archivo_configuracion "${dominio}" "${ip}"
            regenerar_spf "${dominio}" "${ip}" "${usuario_cpanel}"
        fi
    done

    # Se realiza lo mismo con los subdominios
    for subdominio in "${subdominios[@]}"
    do
        if ! grep -Fq "${subdominio}" "${tmp_mailips}";
        then
            escribe_archivo_configuracion "${subdominio}" "${ip}"
            match "${dominios_cpanel[@]}" "${subdominio}"
            regenerar_spf "${dominio}" "${ip}" "${usuario_cpanel}" "${prefijo}"
            unset prefijo
        fi
    done

    unset dominios_cpanel
    unset subdominios
}
# ========================== Fin funciones auxiliars =================================


# ========================== Lógica del programa =====================================

# Para cada dominio que tenga una IP asignada, se fuerza que envie correo por dicha IP
# Recibe: nada
# Devuelve: nada
function gestionar_dominios() {
    local dominios_con_ip
    local entrada
    local usuario_cpanel
    local dominio
    local ip

    mapfile -t dominios_con_ip < "${dominios}"                                # Se obtienen todos los dominios junto con la IP asignada

    for entrada in "${dominios_con_ip[@]}";
    do
        dominio=$( cut -d : -f 1  <<< "${entrada}")
        ip=$( cut -d " " -f 2 <<< "${entrada}" )
        usuario_cpanel=$( /scripts/whoowns "${dominio}" )

        escribe_archivo_configuracion "${dominio}" "${ip}" "${usuario_cpanel}"
        regenerar_spf "${dominio}" "${ip}" "${usuario_cpanel}"
    done
}

# Para cada cuenta CPanel con una IP asignada, se fuerzan todos los dominios en dicha cuenta a enviar por esa IP
# Recibe: nada
# Devuelve: nada
function gestionar_cpanels() {
    local cpanels_con_ip
    local usuario_cpanel
    local ip
    local entrada

    mapfile -t cpanels_con_ip < "${cpanels}"                                  # Se obtienen las cuentas cpanel a las que se ha asignado una IP

    for entrada in "${cpanels_con_ip[@]}";
    do
        usuario_cpanel=$( cut -d : -f 1 <<< "${entrada}" )
        ip=$( cut -d " " -f 2 <<< "${entrada}" )

        # Se obtienen todos los dominios en una cuenta CPanel
        bucle_principal "${usuario_cpanel}" "${ip}"
    done
}

# Se procesan todos los dominios pertenecientes a un reseller con IP de correo definida
# Recibe: nada
# Devuelve: nada
function gestionar_resellers() {
    local resellers_con_ip
    local cuentas_cpanel
    local usuario_cpanel
    local ip
    local reseller
    local entrada

    mapfile -t resellers_con_ip < "${resellers}"                              # Se obtienen las cuentas de reseller presentes en el servidor con IP de envío definida

    for entrada in "${resellers_con_ip[@]}";
    do
        reseller=$( cut -d : -f 1 <<< "${entrada}" )
        ip=$( cut -d " " -f 2 <<< "${entrada}" )

        bucle_principal "${reseller}" "${ip}"

        # Genera la configuración de envío para las cuentas CPanel pertenecientes a un reseller
        mapfile -t cuentas_cpanel < <( uapi --user="${reseller}" Resellers list_accounts | fgrep user: | awk '{print $2}' )

        for usuario_cpanel in "${cuentas_cpanel}";
        do
            bucle_principal "${usuario_cpanel}" "${ip}"
        done
    done
}

# Se comprueba si existen diferencias entre los nuevos ficheros /etc/mailhelo y /etc/mailips generados por el script y los originales. En caso de existir diferencias, hace backup de los originales y los sustituye. Finalmente reinicia exim
# Recibe: nada
# Devuelve: nada
function atualizar_ficheros() {
    local hostname
    local hostname_a
    local ip_principal
    local ip_secundaria
    local ip_envio_actual
    local helo_actual

    hostname=$( hostname -f )
    hostname_a="$( echo "${hostname}" | cut -d \. -f 1 )a.webcloud.es"

    ip_principal=$( dig +short "${hostname}" )
    ip_secundaria=$( dig +short "${hostname_a}" )

    ip_envio_actual=$( grep -E "\*: (${ip_principal}|${ip_secundaria})" /etc/mailips )
    if [[ -z "${ip_envio_actual}" ]];
    then
        ip_envio_actual="*: ${ip_principal}"
    fi

    helo_actual=$( grep -E "\*: (${hostname}|${hostname_a})" /etc/mailhelo )
    if [[ -z "${helo_actual}" ]];
    then
        helo_actual="*: ${hostname}"
    fi

    # Se definen la ip y el helo que se están utilizando actualmente en los ficheros temporales
    echo "${ip_envio_actual}" >> "${tmp_mailips}"
    echo "${helo_actual}" >> "${tmp_mailhelo}"

    # De existir diferencias entre los ficheros actuales y los generados, se hace backup, se sustituyen y se reinicia exim
    if [[ -n $( diff /etc/mailips "${tmp_mailips}" ) ]];
    then
        mv /etc/mailips "${exim_backup_config_files}/mailips_${fecha}"
        mv -f "${tmp_mailips}" /etc/mailips
        mv /etc/mailhelo "${exim_backup_config_files}/mailips_${fecha}"
        mv -f "${tmp_mailhelo}" /etc/mailhelo
        /scripts/restartsrv_exim &> /dev/null
    else
        rm -f "${tmp_mailips}"
        rm -f "${tmp_mailhelo}"
    fi
}

# ========================== Fin de la lógica del programa ===========================

function main() {
    [[ -d "${exim_backup_config_files}" ]] || mkdir -p "${exim_backup_config_files}"
    tmp_mailips=$( mktemp "${exim_backup_config_files}"/mailips.XXXXXXX )                 # mailips temporal
    tmp_mailhelo=$( mktemp "${exim_backup_config_files}"/mailhelo.XXXXXXX )               # mailhelo temporal
    gestionar_dominios
    gestionar_cpanels
    gestionar_resellers
    atualizar_ficheros
}

main
