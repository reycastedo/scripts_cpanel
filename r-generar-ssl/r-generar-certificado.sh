#!/bin/bash

##############################################################
# Datos a rellenar para emitir el certificado
common_name="${1}"             # ej: dominio para el que se emite, con www
pais="${2}"                    # ej: ES
estado="${3}"                  # ej: Galicia
localidad="${4}"               # ej: Lugo
email="${5}"                   # ej: cuenta de correo del cliente
organizacion="${6}"            # ej: Empresa de pruebas
unidad_organizativa="${7}"     # ej: IT - seccion de la empresa, normalmente en blanco
################################################################

path=$( pwd )
singing_request="${path}/${common_name}.csr"
clave="${path}/${common_name}.key"
certificado="${path}/${common_name}.crt"

if ! [[ "${common_name}" =~ www\.[a-z-]+\.[a-z]+ ]] && ! [[ "${common_name}" =~ [a-z-]+\.[a-z-]+\.[a-z]+ ]]; then
    echo "Uso: ${0} www.dominio.tld [ país ] [ estado ] [ localidad ] [ email ] [ organización ] [ unidad organizativa ]"
    echo
    echo "pais                = ES"
    echo "estado              = Galicia"
    echo "localidad           = Lugo"
    echo "email               = info@raiolanetworks.es"
    echo "organización        = Raiola Networks"
    echo "unidad organizativa = IT"
    echo
    exit 1
else
    # se genera una clave privada
    openssl genrsa -out "${clave}" 4096 &>/dev/null

    # Se genera el csr en base a la clave
    openssl req -new -nodes \
        -key "${clave}" \
        -out "${singing_request}" \
        -days 3650 \
        -subj "/C=${pais}/ST=${estado}/L=${localidad}/O=${organizacion}/OU=${unidad_organizativa}/emailAddress=${email}/CN=${common_name}" &>/dev/null
    exit 0
fi
